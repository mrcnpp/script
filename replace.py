import os
replacement = ""
for dname, dirs, files in os.walk("/home/marco/Desktop/subnet.txt"):
    for fname in files:
        fpath = os.path.join(dname, fname)
        with open(fpath) as f:
            s = f.read()
        s = s.replace("-", replacement)
        with open(fpath, "w") as f:
            f.write(s)
